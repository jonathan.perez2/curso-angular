import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { AgregarTarea } from '../models/agregar-tarea.model';

@Component({
  selector: 'app-lista-tarea',
  templateUrl: './lista-tarea.component.html',
  styleUrls: ['./lista-tarea.component.css']
})
export class ListaTareaComponent implements OnInit {

	@Input() tarea: AgregarTarea;
	@HostBinding('attr.class') cssClass = 'col-md-4';

	constructor() { }

  	ngOnInit(): void {
  	}

}
