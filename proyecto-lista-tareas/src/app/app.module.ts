import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AgregarTareaComponent } from './agregar-tarea/agregar-tarea.component';
import { ListaTareaComponent } from './lista-tarea/lista-tarea.component';

@NgModule({
  declarations: [
    AppComponent,
    AgregarTareaComponent,
    ListaTareaComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
