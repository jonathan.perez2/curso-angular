import { Component, OnInit } from '@angular/core';
import { AgregarTarea } from '../models/agregar-tarea.model';

@Component({
  selector: 'app-agregar-tarea',
  templateUrl: './agregar-tarea.component.html',
  styleUrls: ['./agregar-tarea.component.css']
})
export class AgregarTareaComponent implements OnInit {
	
	title = "Lista de tareas";
  tareas: AgregarTarea[];

  constructor() {
    this.tareas = [];
  }

  ngOnInit(): void {
  }

  guardar(tituloTarea:string, textoTarea:string):boolean{
  	this.tareas.push(new AgregarTarea(tituloTarea, textoTarea));
    console.log(this.tareas);
  	return false;
  }
}
